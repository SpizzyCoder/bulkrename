use std::ffi::OsString;
use std::fs;

use clap::Parser;

/// Simple program to rename multiple files
#[derive(Parser, Debug)]
#[clap(version, about)]
pub struct Args {
    /// Search string
    #[arg()]
    search_str: String,

    /// Replace string
    #[arg()]
    replace_str: String,

    /// Include hidden files
    #[arg(short, long)]
    include_hidden: bool,
}

fn main() {
    let args: Args = Args::parse();

    let objects_as_strings: Vec<String> = match fs::read_dir(".") {
        Ok(iterator) => iterator
            .filter_map(|x| x.ok()) // Get all the valid items
            .filter_map(|x| convert_osstring_to_string(x.file_name()))
            .collect(),
        Err(error) => {
            eprintln!["Failed to open the current dir [Error: {}]", error];
            return;
        }
    };

    for cur_object in objects_as_strings {
        if cur_object.starts_with(".") && args.include_hidden == false {
            continue;
        }

        let renamed_object: String = cur_object.replace(&args.search_str, &args.replace_str);

        // Ignore not renamed objects
        if renamed_object == cur_object {
            continue;
        }

        // Rename the object
        match fs::rename(&cur_object, &renamed_object) {
            Ok(_) => println!["{} -> {}", cur_object, renamed_object],
            Err(error) => eprintln![
                "ERROR {} -> {} [Error: {}]",
                cur_object, renamed_object, error
            ],
        };
    }
}

fn convert_osstring_to_string(string: OsString) -> Option<String> {
    match string.to_str() {
        Some(string) => return Some(string.to_owned()),
        None => return None,
    };
}